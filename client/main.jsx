import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import _ from 'lodash';
import toastr from 'toastr';
import App from './components/App';
import displayError from './helpers/display-error';
import 'semantic-ui-css/semantic.min.css';
import 'toastr/build/toastr.min.css';
import './styles/standard.css';

const debug = require('debug')('client-error');

toastr.options = {
  progressBar: true,
  positionClass: 'toast-top-center',
};

window.addEventListener('error', e => {
  debug('window error', e);
  const message = !_.isNil(e.error) ? e.error.toString() : e.message;

  displayError(message);
});

const render = () => ReactDOM.render(
  <AppContainer>
    <App />
  </AppContainer>,
  document.getElementById('root'),
);

render(App);

if (module.hot) {
  module.hot.accept('./components/App', () => {
    render(App);
  });
}
