import React from 'react';
import { Button } from 'semantic-ui-react';
// import { Grid } from 'semantic-ui-react'
import NewClient from './NewClient';
import Deposit from './Deposit';
import Withdraw from './Withdraw';
import Transfer from './Transfer';
import AccoutInfo from './AccountInfo';

class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showNewClientAccount: false,
      showDeposit: false,
      showWithdraw: false,
      showTransfer: false,
      showAccountInfo: false,
    };
    this.toggleNewClientAccountShow = this.toggleNewClientAccountShow.bind(this);
    this.toggleDepositShow = this.toggleDepositShow.bind(this);
    this.toggleWithdrawShow = this.toggleWithdrawShow.bind(this);
    this.toggleTransferShow = this.toggleTransferShow.bind(this);
    this.toggleAccountInfoShow = this.toggleAccountInfoShow.bind(this);
  }

  toggleNewClientAccountShow = () => {
    this.setState(prevState => ({ showNewClientAccount: !prevState.showNewClientAccount }));
  }

  toggleDepositShow = () => {
    this.setState(prevState => ({ showDeposit: !prevState.showDeposit }));
  }

  toggleWithdrawShow = () => {
    this.setState(prevState => ({ showWithdraw: !prevState.showWithdraw }));
  }

  toggleTransferShow = () => {
    this.setState(prevState => ({ showTransfer: !prevState.showTransfer }));
  }

  toggleAccountInfoShow = () => {
    this.setState(prevState => ({ showAccountInfo: !prevState.showAccountInfo }));
  }

  render() {
    const {
      showNewClientAccount, showDeposit, showWithdraw, showTransfer,
      showAccountInfo,
    } = this.state;
    const {
      toggleNewClientAccountShow, toggleDepositShow, toggleWithdrawShow,
      toggleTransferShow, toggleAccountInfoShow,
    } = this;
    return (
      <div>
        <h1>Welcome to One Bank!</h1>
        <br />
        <Button primary onClick={this.toggleNewClientAccountShow}>
          New Client/Account
        </Button>
        <Button primary onClick={this.toggleDepositShow}>
          Deposit
        </Button>
        <Button primary onClick={this.toggleWithdrawShow}>
          Withdraw
        </Button>
        <Button primary onClick={this.toggleTransferShow}>
          Transfer
        </Button>
        <Button primary onClick={this.toggleAccountInfoShow}>
          Account Information
        </Button>
        <NewClient show={showNewClientAccount}
          toggleModalShow={toggleNewClientAccountShow}
        />
        <Deposit show={showDeposit}
          toggleModalShow={toggleDepositShow}
        />
        <Withdraw show={showWithdraw}
          toggleModalShow={toggleWithdrawShow}
        />
        <Transfer show={showTransfer}
          toggleModalShow={toggleTransferShow}
        />
        <AccoutInfo show={showAccountInfo}
          toggleModalShow={toggleAccountInfoShow}
        />
      </div>
    );
  }
}

export default Index;
