import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {
  Modal, Form, Input, Button,
} from 'semantic-ui-react';
import displaySuccess from '../../helpers/display-success';
import createClient from '../../api/newClientApi';

const account = {
  accountId: '',
  balance: 0,
};

const defaultValues = {
  clientId: '',
  name: '',
  email: '',
  phoneNumber: '',
  accounts: [],
};

class NewClient extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.state = _.assignIn(this.state, defaultValues);
  }

  handleChange = (event, { name, value }) => {
    this.setState(prevState => {
      const oldState = _.cloneDeep(prevState);
      _.set(oldState, name, value);
      console.log(oldState);
      return oldState;
    });
  }

  handleSubmit = async (event) => {
    const { toggleModalShow } = this.props;
    event.preventDefault();
    if (await createClient(this.state)) {
      toggleModalShow();
      this.setState(defaultValues);
      displaySuccess('Client/Acount created successfully');
    }
  }

  handleAddAccount = () => {
    this.setState(prevState => ({
      ...prevState, accounts: [...prevState.accounts, account],
    }));
  }

  handleRemoveAccount = () => {
    this.setState(prevState => {
      const prevAccounts = _.cloneDeep(prevState.accounts);
      prevAccounts.pop();
      return (
        {
          ...prevState, accounts: prevAccounts,
        });
    });
  }

  render() {
    const {
      clientId, name, email, phoneNumber, accounts,
    } = this.state;
    const { handleChange, handleAddAccount, handleRemoveAccount } = this;
    const { show, toggleModalShow } = this.props;
    return (
      <Modal open={show}
        onOpen={toggleModalShow}
        onClose={toggleModalShow}
        size="tiny"
        closeOnDimmerClick={false}
        closeOnDocumentClick={false}
      >
        <Modal.Header>Create Client And Account</Modal.Header>
        <Modal.Content scrolling>
          <Modal.Description>
            <Form>
              <Form.Field
                name="clientId"
                control={Input}
                type="number"
                value={clientId}
                label="Client ID"
                onChange={handleChange}
                required
              />
              <Form.Field
                name="name"
                control={Input}
                value={name}
                label="Client Name"
                onChange={handleChange}
                required
              />
              <Form.Field
                name="email"
                control={Input}
                value={email}
                label="Client Email"
                onChange={handleChange}
                required
              />
              <Form.Field
                name="phoneNumber"
                control={Input}
                value={phoneNumber}
                label="Client Phone Number"
                onChange={handleChange}
                required
              />
              {
                accounts.map((value, key) => (
                  // eslint-disable-next-line react/no-array-index-key
                  <div key={key}>
                    <label htmlFor={`Account-${key}-label`}>
                      <h3>{`Account #${key + 1}`}</h3>
                    </label>
                    <br />
                    <Form.Field
                      name={`accounts[${key}].accountId`}
                      control={Input}
                      type="number"
                      value={value.accountId}
                      label="Account ID"
                      onChange={handleChange}
                      required
                    />
                    <Form.Field
                      name={`accounts[${key}].balance`}
                      control={Input}
                      type="number"
                      value={value.balance}
                      label="balance (CAD)"
                      onChange={handleChange}
                      required
                    />
                  </div>
                ))
              }
              <Form.Group widths="equal">
                <Button primary onClick={handleAddAccount}>Add Account</Button>
                {
                  accounts.length > 0
                    ? <Button primary onClick={handleRemoveAccount}>Remove Account</Button>
                    : null
                }
              </Form.Group>
            </Form>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button color="red" onClick={toggleModalShow}>
            Cancel
          </Button>
          <Button color="green" onClick={this.handleSubmit}>
            Submit
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

NewClient.propTypes = {
  show: PropTypes.bool.isRequired,
  toggleModalShow: PropTypes.func.isRequired,
};

export default NewClient;
