import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {
  Modal, Form, Input, Button,
} from 'semantic-ui-react';
import getAccount from '../../api/accountApi';
import displayWarning from '../../helpers/display-warning';

const defaultValues = {
  accountId: '',
  balance: null,
};

class AccountInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.state = _.assignIn(this.state, defaultValues);
  }

  handleChange = (event, { name, value }) => {
    this.setState({ [name]: value });
  }

  handleSubmit = async (event) => {
    const { accountId } = this.state;
    event.preventDefault();
    const accountInfo = await getAccount(this.state);
    if (_.get(accountInfo, 'balance')) {
      this.setState({ balance: accountInfo.balance });
    } else {
      displayWarning(`Account ${accountId} doesn't exist!`);
    }
  }

  handleClose = () => {
    const { toggleModalShow } = this.props;
    this.setState(defaultValues);
    toggleModalShow();
  }

  render() {
    const { accountId, balance } = this.state;
    const { handleChange, handleClose } = this;
    const { show, toggleModalShow } = this.props;
    return (
      <Modal open={show}
        onOpen={toggleModalShow}
        onClose={toggleModalShow}
        size="tiny"
        closeOnDimmerClick={false}
        closeOnDocumentClick={false}
      >
        <Modal.Header>Account Information</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <Form>
              <Form.Field
                name="accountId"
                control={Input}
                type="number"
                value={accountId}
                label="Account ID"
                onChange={handleChange}
                required
              />
              {
                balance
                  ? (
                    <h3>{`Balance: $${balance} CAD`}</h3>
                  )
                  : null
              }
            </Form>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          {
            !balance
              ? (
                <Button color="red" onClick={toggleModalShow}>
                  Cancel
                </Button>
              )
              : (
                <Button color="red" onClick={handleClose}>
                  Close
                </Button>
              )
          }
          <Button color="green" onClick={this.handleSubmit}>
            Submit
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

AccountInfo.propTypes = {
  show: PropTypes.bool.isRequired,
  toggleModalShow: PropTypes.func.isRequired,
};

export default AccountInfo;
