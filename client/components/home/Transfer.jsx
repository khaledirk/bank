import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {
  Modal, Form, Input, Button, Select,
} from 'semantic-ui-react';
import displaySuccess from '../../helpers/display-success';
import transfer from '../../api/transferApi';

const defaultValues = {
  clientId: '',
  fromAccountId: '',
  toAccountId: '',
  amount: 0,
  currency: 'CAD',
};

class Transfer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.state = _.assignIn(this.state, defaultValues);
  }

  handleChange = (event, { name, value }) => {
    this.setState({ [name]: value });
  }

  handleSubmit = async (event) => {
    const { toggleModalShow } = this.props;
    event.preventDefault();
    if (await transfer(this.state)) {
      toggleModalShow();
      this.setState(defaultValues);
      displaySuccess('Transfer completed successfully');
    }
  }

  render() {
    const {
      clientId, fromAccountId, toAccountId, amount, currency,
    } = this.state;
    const { handleChange } = this;
    const { show, toggleModalShow } = this.props;
    const currencyOptions = [
      { key: 1, text: 'CAD', value: 'CAD' },
      { key: 2, text: 'USD', value: 'USD' },
      { key: 3, text: 'MXN', value: 'MXN' },
    ];
    return (
      <Modal open={show}
        onOpen={toggleModalShow}
        onClose={toggleModalShow}
        size="tiny"
        closeOnDimmerClick={false}
        closeOnDocumentClick={false}
      >
        <Modal.Header>Transfer</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <Form>
              <Form.Field
                name="clientId"
                control={Input}
                type="number"
                value={clientId}
                label="Client ID"
                onChange={handleChange}
                required
              />
              <Form.Field
                name="fromAccountId"
                control={Input}
                type="number"
                value={fromAccountId}
                label="From Account ID"
                onChange={handleChange}
                required
              />
              <Form.Field
                name="toAccountId"
                control={Input}
                type="number"
                value={toAccountId}
                label="To Account ID"
                onChange={handleChange}
                required
              />
              <Form.Group widths="equal">
                <Form.Field
                  name="amount"
                  control={Input}
                  type="number"
                  value={amount}
                  label="Amount"
                  onChange={handleChange}
                  required
                />
                <Form.Field
                  name="currency"
                  control={Select}
                  value={currency}
                  options={currencyOptions}
                  label="Currency"
                  onChange={handleChange}
                />
              </Form.Group>
            </Form>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button color="red" onClick={toggleModalShow}>
            Cancel
          </Button>
          <Button color="green" onClick={this.handleSubmit}>
            Submit
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

Transfer.propTypes = {
  show: PropTypes.bool.isRequired,
  toggleModalShow: PropTypes.func.isRequired,
};

export default Transfer;
