import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {
  Modal, Form, Input, Button, Select,
} from 'semantic-ui-react';
import displaySuccess from '../../helpers/display-success';
import withdraw from '../../api/withdrawApi';

const defaultValues = {
  clientId: '',
  accountId: '',
  amount: 0,
  currency: 'CAD',
};

class Withdraw extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.state = _.assignIn(this.state, defaultValues);
  }

  handleChange = (event, { name, value }) => {
    this.setState({ [name]: value });
  }

  handleSubmit = async (event) => {
    const { toggleModalShow } = this.props;
    event.preventDefault();
    if (await withdraw(this.state)) {
      toggleModalShow();
      this.setState(defaultValues);
      displaySuccess('Withdraw completed successfully');
    }
  }

  render() {
    const {
      clientId, accountId, amount, currency,
    } = this.state;
    const { handleChange } = this;
    const { show, toggleModalShow } = this.props;
    const currencyOptions = [
      { key: 1, text: 'CAD', value: 'CAD' },
      { key: 2, text: 'USD', value: 'USD' },
      { key: 3, text: 'MXN', value: 'MXN' },
    ];
    return (
      <Modal open={show}
        onOpen={toggleModalShow}
        onClose={toggleModalShow}
        size="tiny"
        closeOnDimmerClick={false}
        closeOnDocumentClick={false}
      >
        <Modal.Header>Withdraw</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <Form>
              <Form.Field
                name="clientId"
                control={Input}
                type="number"
                value={clientId}
                label="Client ID"
                onChange={handleChange}
                required
              />
              <Form.Field
                name="accountId"
                control={Input}
                type="number"
                value={accountId}
                label="Account ID"
                onChange={handleChange}
                required
              />
              <Form.Group widths="equal">
                <Form.Field
                  name="amount"
                  control={Input}
                  type="number"
                  value={amount}
                  label="Amount"
                  onChange={handleChange}
                  required
                />
                <Form.Field
                  name="currency"
                  control={Select}
                  value={currency}
                  options={currencyOptions}
                  label="Currency"
                  onChange={handleChange}
                />
              </Form.Group>
            </Form>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button color="red" onClick={toggleModalShow}>
            Cancel
          </Button>
          <Button color="green" onClick={this.handleSubmit}>
            Submit
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

Withdraw.propTypes = {
  show: PropTypes.bool.isRequired,
  toggleModalShow: PropTypes.func.isRequired,
};

export default Withdraw;
