import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import HomePage from './home/Index';
import PageNotFound from './common/PageNotFound';

function App() {
  return (
    <div className="container-fluid center">
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route component={PageNotFound} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
