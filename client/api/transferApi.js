import request from 'superagent';
import displayError from '../helpers/display-error';

export default async function (data) {
  const {
    clientId, fromAccountId, toAccountId, amount, currency,
  } = data;

  try {
    await request
      .put(`/api/transfer/${clientId}/${fromAccountId}/`
        + `${toAccountId}?amount=${amount}&currency=${currency}`);
    return true;
  } catch (err) {
    displayError(err);
  }
}
