import request from 'superagent';
import displayError from '../helpers/display-error';

export default async function (data) {
  const {
    accountId, amount, currency,
  } = data;

  try {
    await request.put(`/api/deposit/${accountId}?amount=${amount}&currency=${currency}`);
    return true;
  } catch (err) {
    displayError(err);
  }
}
