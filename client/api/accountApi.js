import request from 'superagent';
import displayError from '../helpers/display-error';

export default async function (data) {
  const { accountId } = data;

  try {
    const res = await request.get(`/api/accounts/${accountId}`);
    return res.body;
  } catch (err) {
    displayError(err);
  }
}
