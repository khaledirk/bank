import request from 'superagent';
import _ from 'lodash';
import displayError from '../helpers/display-error';

export default async function (data) {
  console.log(data);
  const {
    clientId, name, email, phoneNumber, accounts,
  } = data;
  const clientData = {
    clientId, name, email, phoneNumber,
  };
  try {
    await request.post('/api/clients').send(clientData);
    _.forEach(accounts, async account => {
      const { accountId } = account;
      await request.post('/api/accounts').send(account);
      await request.put(`/api/associate/${clientId}/${accountId}`);
    });
    return true;
  } catch (err) {
    displayError(err);
  }
}
