import _ from 'lodash';
import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default (state = initialState.clients, action) => {
  switch (action.type) {
    case types.LOAD_CLIENT_SUCCESS:
      return action.clients;
    case types.CREATE_CLIENT_SUCCESS: {
      const clientCopy = _.cloneDeep(state);
      clientCopy.push(action.clients);
      return clientCopy;
    }
    case types.UPDATE_CLIENT_SUCCESS: {
      const clientCopy = _.cloneDeep(state);
      const index = _.findIndex(clientCopy, client => client.id === action.client.id);
      if (index >= 0) {
        clientCopy[index] = action.client;
      }
      return clientCopy;
    }
    default:
      return state;
  }
};
