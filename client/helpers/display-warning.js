import toastr from 'toastr';

export default function (message) {
  return toastr.warning(message);
}
