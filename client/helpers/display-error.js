import toastr from 'toastr';
import _ from 'lodash';

export default function (err) {
  const message = (_.get(err, 'response.body.error.title') || err.message || err
    || 'unknown error');
  return toastr.error(message);
}
