import config from 'config';
import _ from 'lodash';

class Account {
  constructor(id, balance) {
    this.id = id;
    this.balance = _.toNumber(balance);
    this.clientIds = [];
  }

  // This check is to avoid assigning same client multiple times to same account
  isClientExist(clientId) {
    return _.find(this.clientIds, ClId => ClId === clientId);
  }

  assignClient(clientId) {
    this.clientIds.push(clientId);
  }

  deposit(amount, curreny) {
    // Perform input validation before processing the transaction
    if (!config.exchangeRate[curreny]) {
      throw new Error(`This bank doesn't accept ${curreny} curreny`);
    }
    if (_.toNumber(amount) < 1) {
      throw new Error('Invalid deposit amount. Amount should be greater than 0');
    }
    this.balance += _.toNumber(amount) / config.exchangeRate[curreny];
  }

  withdraw(amount, curreny) {
    // Perform input validation before processing the transaction
    if (!config.exchangeRate[curreny]) {
      throw new Error(`This bank doesn't accept ${curreny} curreny`);
    }
    if (_.toNumber(amount) < 1) {
      throw new Error('Invalid withdraw amount. Amount should be greater than 0');
    }
    if (this.balance < _.toNumber(amount) / config.exchangeRate[curreny]) {
      throw new Error('insufficient fund');
    }
    this.balance -= _.toNumber(amount) / config.exchangeRate[curreny];
  }
}

export default Account;
