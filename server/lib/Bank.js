import _ from 'lodash';
import config from 'config';
import Client from './Client';
import Account from './Account';

const log = config.log();

class Bank {
  constructor() {
    this.clientList = [];
    this.accountList = [];
  }

  createClient(data) {
    const { clientId, name, email, phoneNumber } = data;
    if (!clientId || !name || !email || !phoneNumber) {
      throw new Error('Invalid user input!')
    }
    if (this.getClient(clientId)) {
      throw new Error(`Client ${clientId} already exists!`);
    }
    const newClient = new Client(clientId, name, email, phoneNumber);
    this.clientList.push(newClient);
    log.debug(`Client ${clientId} created`);
    return newClient;
  }

  createAccount(data) {
    const { accountId, balance } = data;
    if (!accountId || !balance) {
      throw new Error('Invalid user input!');
    }
    if (this.getAccount(accountId)) {
      throw new Error(`Account ${accountId} already exists!`);
    }
    const newAccount = new Account(accountId, balance);
    this.accountList.push(newAccount);
    log.debug(`Account ${accountId} created`);
    return newAccount;
  }

  associateClientWithAccount(clientId, accountId) {
    const client = this.getClient(clientId);
    const account = this.getAccount(accountId);
    if (!client) {
      throw new Error(`Client ${clientId} doesn't exist!`);
    }
    if (!account) {
      throw new Error(`Account ${accountId} doesn't exist!`);
    }
    if (client.isAccountExist(accountId)) {
      throw new Error(`Account ${accountId} already belong to Client ${clientId}`);
    }
    if (account.isClientExist(clientId)) {
      throw new Error(`Client ${clientId} already assigned Account ${accountId}`);
    }
    client.assignAccount(accountId);
    account.assignClient(clientId);
    log.debug(`Account ${accountId} and Client ${clientId} are associated`);
    return client;
  }

  transfer(clientId, fromAccountId, toAccountId, amount, currency) {
    if(fromAccountId === toAccountId){
      throw new Error('Cannot do money transfer within the same account');
    }
    this.withdraw(clientId, fromAccountId, amount, currency);
    try {
      this.deposit(toAccountId, amount, currency);
    } catch (err) {
      // rollback transaction
      this.deposit(fromAccountId, amount, currency);
      throw err;
    }
  }

  deposit(accountId, amount, curreny) {
    const account = this.getAccount(accountId);
    if (!account) {
      throw new Error(`Account ${accountId} doesn't exist!`);
    }
    if (!amount || !curreny) {
      throw new Error('Invalid user input!');
    }
    account.deposit(amount, curreny);
    log.debug(`Amount ${amount} deposited in account ${accountId}`);
    return account;
  }

  withdraw(clientId, accountId, amount, curreny) {
    const client = this.getClient(clientId);
    const account = this.getAccount(accountId);
    if (!client) {
      throw new Error(`Client ${clientId} doesn't exist!`);
    }
    if (!account) {
      throw new Error(`Account ${accountId} doesn't exist!`);
    }
    if (!amount || !curreny) {
      throw new Error('Invalid user input!');
    }
    if (!client.isAccountExist(accountId)) {
      throw new Error('Unauthorized transaction!')
    }
    account.withdraw(amount, curreny);
    log.debug(`Amount ${amount} withdrawn from account ${accountId}`);
    return account;
  }

  getClient(clientId) {
    return _.find(this.clientList, client => client.id === clientId);
  }

  getAccount(accountId) {
    return _.find(this.accountList, account => account.id === accountId);
  }

}

export default Bank;
