import _ from 'lodash';

class Client {
  constructor(id, name, email, phoneNumber) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.phoneNumber = phoneNumber;
    this.accountIds = [];
  }

  // This check is to avoid assigning same account multiple times to same client
  isAccountExist(accountId) {
    return _.find(this.accountIds, accId => accId === accountId);
  }

  assignAccount(accountId) {
    this.accountIds.push(accountId);
  }

}

export default Client;
