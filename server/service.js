import express from 'express';
import HttpStatus from 'http-status-codes';
import createError from 'http-errors';
import bodyParser from 'body-parser';
import path from 'path';
import Bank from './lib/Bank';

const PUBLIC_DIR = path.join(__dirname, '../public');
const HTML_FILE = path.join(PUBLIC_DIR, '/index.html');

const service = express();

module.exports = (config) => {
  const log = config.log();
  const bank = new Bank();

  service.use(express.static(PUBLIC_DIR));
  service.use(bodyParser.urlencoded({ extended: false }));
  service.use(bodyParser.json({ limit: '50mb', type: '*/*' }));

  // Add a request logging middleware
  service.use((req, res, next) => {
    const serviceip = req.connection.remoteAddress.includes('::')
      ? `[${req.connection.remoteAddress}]` : req.connection.remoteAddress;
    log.debug(`${serviceip} ${req.method}: ${decodeURI(req.url)} ${JSON.stringify(req.body)}`);
    return next();
  });

  // Redirect to UI if no /api included in the url
  service.use((req, res, next) => {
    if (req.path.includes('/api')) return next();
    res.sendFile(HTML_FILE);
  });

  // get client info by a given client id
  service.get('/api/clients/:clientid', (req, res, next) => {
    try {
      const { clientid } = req.params;
      const result = bank.getClient(clientid);
      res.send(result);
    } catch (e) {
      next(e);
    }
  });

  // get account info by a given account id
  service.get('/api/accounts/:accountid', (req, res, next) => {
    try {
      const { accountid } = req.params;
      const result = bank.getAccount(accountid);
      res.send(result);
    } catch (e) {
      next(e);
    }
  });

  // withdraw amount from the given account by the given client
  service.put('/api/withdraw/:clientid/:accountid', (req, res, next) => {
    try {
      const { clientid, accountid } = req.params;
      const { amount, currency } = req.query;
      const result = bank.withdraw(clientid, accountid, amount, currency);
      res.send(result);
    } catch (e) {
      next(e);
    }
  });

  // deposit amount in the given account
  service.put('/api/deposit/:accountid', (req, res, next) => {
    try {
      const { accountid } = req.params;
      const { amount, currency } = req.query;
      const result = bank.deposit(accountid, amount, currency);
      res.send(result);
    } catch (e) {
      next(e);
    }
  });

    // transfer amount from given account to another one
    service.put('/api/transfer/:clientid/:fromaccountid/:toaccountid', (req, res, next) => {
      try {
        const { clientid, fromaccountid, toaccountid } = req.params;
        const { amount, currency } = req.query;
        const result = bank.transfer(clientid, fromaccountid, toaccountid, amount, currency);
        res.send(result);
      } catch (e) {
        next(e);
      }
    });

  // associate client with given account
  service.put('/api/associate/:clientid/:accountid', (req, res, next) => {
    try {
      const { clientid, accountid } = req.params;
      const result = bank.associateClientWithAccount(clientid, accountid);
      res.send(result);
    } catch (e) {
      next(e);
    }
  });
  // create new client
  service.post('/api/clients', (req, res, next) => {
    try {
      const data = req.body;
      const result = bank.createClient(data);
      res.send(result);
    } catch (e) {
      next(e);
    }
  });

  // create new account
  service.post('/api/accounts', (req, res, next) => {
    try {
      const data = req.body;
      const result = bank.createAccount(data);
      res.send(result);
    } catch (e) {
      next(e);
    }
  });

  service.use((req, res, next) => next(createError(HttpStatus.NOT_FOUND, `Not found: ${req.url}`)));

  // eslint-disable-next-line no-unused-vars
  service.use((err, req, res, next) => {
    const status = err.status || 500;
    const errorMessage = (err.message || err);
    log.error(err);
    res.status(status).json({
      error: {
        code: status,
        title: errorMessage,
      },
    });
  });
  return service;
};
