const request = require('supertest');
const { service, server } = require('../bin/run');

beforeAll(async () => {
  // setup case 2 with initial data
  await request(service).post('/api/clients')
    .set('content-type', 'application/json')
    .send({ "clientId": "504", "name": "Glenn Quagmire", "email": "Glenn.Quagmire@gmail.com", "phoneNumber": "4038789800" });
  await request(service).post('/api/accounts')
    .set('content-type', 'application/json')
    .send({ "accountId": "2001", "balance": 35000 });
  await request(service).put('/api/associate/504/2001');
});

afterAll(() => server.close());

describe('Test bank transactions', () => {
  it('Glenn Quagmire should end up with balance equal to $9,800 CAD', async () => {
    await request(service).put('/api/withdraw/504/2001?amount=5000&currency=MXN');
    await request(service).put('/api/withdraw/504/2001?amount=12500&currency=USD');
    await request(service).put('/api/deposit/2001?amount=300&currency=CAD');
    const res = await request(service).get('/api/accounts/2001');
    expect(res.statusCode).toBe(200);
    expect(res.body.balance).toBe(9800);
  });
});
