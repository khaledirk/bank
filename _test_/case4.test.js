const request = require('supertest');
const { service, server } = require('../bin/run');

beforeAll(async () => {
  // setup case 4 with initial data
  await request(service).post('/api/clients')
    .set('content-type', 'application/json')
    .send({ "clientId": "123", "name": "Peter Griffin", "email": "Peter.Griffin@gmail.com", "phoneNumber": "4035589451" });
  await request(service).post('/api/accounts')
    .set('content-type', 'application/json')
    .send({ "accountId": "0123", "balance": 150 });
  await request(service).put('/api/associate/123/0123');
  await request(service).post('/api/clients')
    .set('content-type', 'application/json')
    .send({ "clientId": "456", "name": "Lois Griffin", "email": "Lois.Griffin@gmail.com", "phoneNumber": "4035589450" });
  await request(service).post('/api/accounts')
    .set('content-type', 'application/json')
    .send({ "accountId": "0456", "balance": 65000 });
  await request(service).put('/api/associate/456/0456');
});

afterAll(() => server.close());

describe('Test bank transactions', () => {
  it('Peter Griffin account 0123 balance should become $33.75 CAD, Lois Griffin account 0456 balance should become $112,554.25 CAD', async () => {
    await request(service).put('/api/withdraw/123/0123?amount=70&currency=USD');
    await request(service).put('/api/deposit/0456?amount=23789&currency=USD');
    await request(service).put('/api/transfer/456/0456/0123?amount=23.75&currency=CAD');

    let res = await request(service).get('/api/accounts/0123');
    expect(res.statusCode).toBe(200);
    expect(res.body.balance).toBe(33.75);

    res = await request(service).get('/api/accounts/0456');
    expect(res.statusCode).toBe(200);
    expect(res.body.balance).toBe(112554.25);
  });
});
