const request = require('supertest');
const { service, server } = require('../bin/run');

beforeAll(async () => {
  // setup case 5 with initial data
  await request(service).post('/api/clients')
    .set('content-type', 'application/json')
    .send({ "clientId": "002", "name": "Joe Swanson", "email": "Joe.Swanson@gmail.com", "phoneNumber": "4034589811" });
  await request(service).post('/api/accounts')
    .set('content-type', 'application/json')
    .send({ "accountId": "1010", "balance": 7425 });
  await request(service).put('/api/associate/002/1010');
  await request(service).post('/api/clients')
    .set('content-type', 'application/json')
    .send({ "clientId": "219", "name": "John Shark", "email": "John.Shark@gmail.com", "phoneNumber": "4034533867" });
});

afterAll(() => server.close());

describe('Test bank transactions', () => {
  it('John Shark attempt to withdraw $100 USD from Joe Swanson account', async () => {
    let res = await request(service).put('/api/withdraw/219/1010?amount=100&currency=CAD');
    expect(res.statusCode).toBe(500);
    expect(res.body.error.title).toContain('Unauthorized transaction');

    res = await request(service).get('/api/accounts/1010');
    expect(res.statusCode).toBe(200);
    expect(res.body.balance).toBe(7425);
  });
});
