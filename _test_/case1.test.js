const request = require('supertest');
const { service, server } = require('../bin/run');

beforeAll(async () => {
  // setup case 1 with initial data
  await request(service).post('/api/clients')
    .set('content-type', 'application/json')
    .send({ "clientId": "777", "name": "Stewie Griffin", "email": "Stewie.Griffin@gmail.com", "phoneNumber": "4038779900" });
  await request(service).post('/api/accounts')
    .set('content-type', 'application/json')
    .send({ "accountId": "1234", "balance": 100 });
  await request(service).put('/api/associate/777/1234');
});

afterAll(() => server.close());

describe('Test bank transactions', () => {
  it('Stewie Griffin should end up with balance equal to $700 CAD', async () => {
    await request(service).put('/api/deposit/1234?amount=300&currency=USD');
    const res = await request(service).get('/api/accounts/1234');
    expect(res.statusCode).toBe(200);
    expect(res.body.balance).toBe(700);
  });
});
