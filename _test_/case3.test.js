const request = require('supertest');
const { service, server } = require('../bin/run');

beforeAll(async () => {
  // setup case 3 with initial data
  await request(service).post('/api/clients')
    .set('content-type', 'application/json')
    .send({ "clientId": "002", "name": "Joe Swanson", "email": "Joe.Swanson@gmail.com", "phoneNumber": "4034589811" });
  await request(service).post('/api/accounts')
    .set('content-type', 'application/json')
    .send({ "accountId": "1010", "balance": 7425 });
  await request(service).put('/api/associate/002/1010');
  await request(service).post('/api/accounts')
    .set('content-type', 'application/json')
    .send({ "accountId": "5500", "balance": 15000 });
  await request(service).put('/api/associate/002/5500');  
});

afterAll(() => server.close());

describe('Test bank transactions', () => {
  it('Joe Swanson account 1010 balance should become $1,497.6 CAD, account 5500 balance should become $17,300 CAD', async () => {
    await request(service).put('/api/withdraw/002/5500?amount=5000&currency=CAD');
    await request(service).put('/api/transfer/002/1010/5500?amount=7300&currency=CAD');
    await request(service).put('/api/deposit/1010?amount=13726&currency=MXN');

    let res = await request(service).get('/api/accounts/1010');
    expect(res.statusCode).toBe(200);
    expect(res.body.balance).toBe(1497.6);

    res = await request(service).get('/api/accounts/5500');
    expect(res.statusCode).toBe(200);
    expect(res.body.balance).toBe(17300);
  });
});
