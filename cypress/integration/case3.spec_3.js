before(() => {
  // Setup case 1 with initial input
  cy.visit('http://localhost:1338');
  cy.get('button').contains('New Client/Account').click();
  cy.get('.modals').should('exist');
  cy.get('.input [name="clientId"]').type('002');
  cy.get('.input [name="name"]').type('Joe Swanson');
  cy.get('.input [name="email"]').type('Joe.Swanson@gmail.com');
  cy.get('.input [name="phoneNumber"]').type('4034589811');
  cy.get('button').contains('Add Account').click();
  cy.get('.input [name="accounts[0].accountId"]').type('1010');
  cy.get('.input [name="accounts[0].balance"]').type('7425');
  cy.get('button').contains('Add Account').click();
  cy.get('.input [name="accounts[1].accountId"]').type('5500');
  cy.get('.input [name="accounts[1].balance"]').type('15000');
  cy.get('button').contains('Submit').click();
  cy.get('.toast-message').contains('Client/Acount created successfully').should('exist');
  cy.get('.toast-message').should('not.exist');
});

describe('Test bank transactions', () => {
  it('Joe Swanson account 1010 balance should become $1,497.6 CAD, account 5500 balance should become $17,300 CAD', () => {
    cy.get('button').contains('Withdraw').click();
    cy.get('.modals').should('exist');
    cy.get('.input [name="clientId"]').type('002');
    cy.get('.input [name="accountId"]').type('5500');
    cy.get('.input [name="amount"]').type('5000');
    cy.get('i.dropdown').click();
    cy.get('[role="option"]').contains('CAD').click();
    cy.get('button').contains('Submit').click();
    cy.get('.toast-message').contains('Withdraw completed successfully').should('exist');
    cy.get('.toast-message').should('not.exist');

    cy.get('button').contains('Transfer').click();
    cy.get('.modals').should('exist');
    cy.get('.input [name="clientId"]').type('002');
    cy.get('.input [name="fromAccountId"]').type('1010');
    cy.get('.input [name="toAccountId"]').type('5500');
    cy.get('.input [name="amount"]').type('7300');
    cy.get('i.dropdown').click();
    cy.get('[role="option"]').contains('CAD').click();
    cy.get('button').contains('Submit').click();
    cy.get('.toast-message').contains('Transfer completed successfully').should('exist');
    cy.get('.toast-message').should('not.exist');

    cy.get('button').contains('Deposit').click();
    cy.get('.modals').should('exist');
    cy.get('.input [name="accountId"]').type('1010');
    cy.get('.input [name="amount"]').type('13726');
    cy.get('i.dropdown').click();
    cy.get('[role="option"]').contains('MXN').click();
    cy.get('button').contains('Submit').click();
    cy.get('.toast-message').contains('Deposit completed successfully').should('exist');
    cy.get('.toast-message').should('not.exist');

    cy.get('button').contains('Account Information').click();
    cy.get('.modals').should('exist');
    cy.get('.input [name="accountId"]').type('1010');
    cy.get('button').contains('Submit').click();
    cy.get('h3').should('contain', '$1497.6');
    cy.get('button').contains('Close').click();

    cy.get('button').contains('Account Information').click();
    cy.get('.modals').should('exist');
    cy.get('.input [name="accountId"]').type('5500');
    cy.get('button').contains('Submit').click();
    cy.get('h3').should('contain', '$17300');
  });
});
