before(() => {
  // Setup case 1 with initial input
  cy.visit('http://localhost:1338');
  cy.get('button').contains('New Client/Account').click();
  cy.get('.modals').should('exist');
  cy.get('.input [name="clientId"]').type('123');
  cy.get('.input [name="name"]').type('Peter Griffin');
  cy.get('.input [name="email"]').type('Peter.Griffin@gmail.com');
  cy.get('.input [name="phoneNumber"]').type('4035589451');
  cy.get('button').contains('Add Account').click();
  cy.get('.input [name="accounts[0].accountId"]').type('0123');
  cy.get('.input [name="accounts[0].balance"]').type('150');
  cy.get('button').contains('Submit').click();
  cy.get('.toast-message').contains('Client/Acount created successfully').should('exist');
  cy.get('.toast-message').should('not.exist');

  cy.get('button').contains('New Client/Account').click();
  cy.get('.modals').should('exist');
  cy.get('.input [name="clientId"]').type('456');
  cy.get('.input [name="name"]').type('Lois Griffin');
  cy.get('.input [name="email"]').type('Lois.Griffin@gmail.com');
  cy.get('.input [name="phoneNumber"]').type('4035589450');
  cy.get('button').contains('Add Account').click();
  cy.get('.input [name="accounts[0].accountId"]').type('0456');
  cy.get('.input [name="accounts[0].balance"]').type('65000');
  cy.get('button').contains('Submit').click();
  cy.get('.toast-message').contains('Client/Acount created successfully').should('exist');
  cy.get('.toast-message').should('not.exist');
});

describe('Test bank transactions', () => {
  it('Peter Griffin account 0123 balance should become $33.75 CAD, Lois Griffin account 0456 balance should become $112,554.25 CAD', () => {
    cy.get('button').contains('Withdraw').click();
    cy.get('.modals').should('exist');
    cy.get('.input [name="clientId"]').type('123');
    cy.get('.input [name="accountId"]').type('0123');
    cy.get('.input [name="amount"]').type('70');
    cy.get('i.dropdown').click();
    cy.get('[role="option"]').contains('USD').click();
    cy.get('button').contains('Submit').click();
    cy.get('.toast-message').contains('Withdraw completed successfully').should('exist');
    cy.get('.toast-message').should('not.exist');

    cy.get('button').contains('Deposit').click();
    cy.get('.modals').should('exist');
    cy.get('.input [name="accountId"]').type('0456');
    cy.get('.input [name="amount"]').type('23789');
    cy.get('i.dropdown').click();
    cy.get('[role="option"]').contains('USD').click();
    cy.get('button').contains('Submit').click();
    cy.get('.toast-message').contains('Deposit completed successfully').should('exist');
    cy.get('.toast-message').should('not.exist');

    cy.get('button').contains('Transfer').click();
    cy.get('.modals').should('exist');
    cy.get('.input [name="clientId"]').type('456');
    cy.get('.input [name="fromAccountId"]').type('0456');
    cy.get('.input [name="toAccountId"]').type('0123');
    cy.get('.input [name="amount"]').type('23.75');
    cy.get('i.dropdown').click();
    cy.get('[role="option"]').contains('CAD').click();
    cy.get('button').contains('Submit').click();
    cy.get('.toast-message').contains('Transfer completed successfully').should('exist');
    cy.get('.toast-message').should('not.exist');

    cy.get('button').contains('Account Information').click();
    cy.get('.modals').should('exist');
    cy.get('.input [name="accountId"]').type('0123');
    cy.get('button').contains('Submit').click();
    cy.get('h3').should('contain', '$33.75');
    cy.get('button').contains('Close').click();

    cy.get('button').contains('Account Information').click();
    cy.get('.modals').should('exist');
    cy.get('.input [name="accountId"]').type('0456');
    cy.get('button').contains('Submit').click();
    cy.get('h3').should('contain', '$112554.25');
  });
});
