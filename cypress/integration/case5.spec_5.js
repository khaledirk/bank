before(() => {
  // Setup case 1 with initial input
  cy.visit('http://localhost:1338');
  cy.get('button').contains('New Client/Account').click();
  cy.get('.modals').should('exist');
  cy.get('.input [name="clientId"]').type('0002');
  cy.get('.input [name="name"]').type('Joe Swanson');
  cy.get('.input [name="email"]').type('Joe.Swanson@gmail.com');
  cy.get('.input [name="phoneNumber"]').type('4034589811');
  cy.get('button').contains('Add Account').click();
  cy.get('.input [name="accounts[0].accountId"]').type('01010');
  cy.get('.input [name="accounts[0].balance"]').type('7425');
  cy.get('button').contains('Submit').click();
  cy.get('.toast-message').contains('Client/Acount created successfully').should('exist');
  cy.get('.toast-message').should('not.exist');

  cy.get('button').contains('New Client/Account').click();
  cy.get('.modals').should('exist');
  cy.get('.input [name="clientId"]').type('219');
  cy.get('.input [name="name"]').type('John Shark');
  cy.get('.input [name="email"]').type('John.Shark@gmail.com');
  cy.get('.input [name="phoneNumber"]').type('4034533867');
  cy.get('button').contains('Submit').click();
  cy.get('.toast-message').contains('Client/Acount created successfully').should('exist');
  cy.get('.toast-message').should('not.exist');
});

describe('Test bank transactions', () => {
  it('John Shark attempt to withdraw $100 USD from Joe Swanson account', () => {
    cy.get('button').contains('Withdraw').click();
    cy.get('.modals').should('exist');
    cy.get('.input [name="clientId"]').type('219');
    cy.get('.input [name="accountId"]').type('01010');
    cy.get('.input [name="amount"]').type('100');
    cy.get('i.dropdown').click();
    cy.get('[role="option"]').contains('CAD').click();
    cy.get('button').contains('Submit').click();
    cy.get('.toast-message').contains('Unauthorized transaction').should('exist');
  });
});
