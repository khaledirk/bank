before(() => {
  // Setup case 1 with initial input
  cy.visit('http://localhost:1338');
  cy.get('button').contains('New Client/Account').click();
  cy.get('.modals').should('exist');
  cy.get('.input [name="clientId"]').type('504');
  cy.get('.input [name="name"]').type('Glenn Quagmire');
  cy.get('.input [name="email"]').type('Glenn.Quagmire@gmail.com');
  cy.get('.input [name="phoneNumber"]').type('4038789800');
  cy.get('button').contains('Add Account').click();
  cy.get('.input [name="accounts[0].accountId"]').type('2001');
  cy.get('.input [name="accounts[0].balance"]').type('35000');
  cy.get('button').contains('Submit').click();
  cy.get('.toast-message').contains('Client/Acount created successfully').should('exist');
  cy.get('.toast-message').should('not.exist');
});

describe('Test bank transactions', () => {
  it('Glenn Quagmire should end up with balance equal to $9,800 CAD', () => {
    cy.get('button').contains('Withdraw').click();
    cy.get('.modals').should('exist');
    cy.get('.input [name="clientId"]').type('504');
    cy.get('.input [name="accountId"]').type('2001');
    cy.get('.input [name="amount"]').type('5000');
    cy.get('i.dropdown').click();
    cy.get('[role="option"]').contains('MXN').click();
    cy.get('button').contains('Submit').click();
    cy.get('.toast-message').contains('Withdraw completed successfully').should('exist');
    cy.get('.toast-message').should('not.exist');

    cy.get('button').contains('Withdraw').click();
    cy.get('.modals').should('exist');
    cy.get('.input [name="clientId"]').type('504');
    cy.get('.input [name="accountId"]').type('2001');
    cy.get('.input [name="amount"]').type('12500');
    cy.get('i.dropdown').click();
    cy.get('[role="option"]').contains('USD').click();
    cy.get('button').contains('Submit').click();
    cy.get('.toast-message').contains('Withdraw completed successfully').should('exist');
    cy.get('.toast-message').should('not.exist');

    cy.get('button').contains('Deposit').click();
    cy.get('.modals').should('exist');
    cy.get('.input [name="accountId"]').type('2001');
    cy.get('.input [name="amount"]').type('300');
    cy.get('i.dropdown').click();
    cy.get('[role="option"]').contains('CAD').click();
    cy.get('button').contains('Submit').click();
    cy.get('.toast-message').contains('Deposit completed successfully').should('exist');
    
    cy.get('button').contains('Account Information').click();
    cy.get('.modals').should('exist');
    cy.get('.input [name="accountId"]').type('2001');
    cy.get('button').contains('Submit').click();
    cy.get('h3').should('contain', '$9800');
  });
});
