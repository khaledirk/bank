before(() => {
  // Setup case 1 with initial input
  cy.visit('http://localhost:1338');
  cy.get('button').contains('New Client/Account').click();
  cy.get('.modals').should('exist');
  cy.get('.input [name="clientId"]').type('777');
  cy.get('.input [name="name"]').type('Stewie Griffin');
  cy.get('.input [name="email"]').type('Stewie.Griffin@gmail.com');
  cy.get('.input [name="phoneNumber"]').type('4038779900');
  cy.get('button').contains('Add Account').click();
  cy.get('.input [name="accounts[0].accountId"]').type('1234');
  cy.get('.input [name="accounts[0].balance"]').type('100');
  cy.get('button').contains('Submit').click();
  cy.get('.toast-message').contains('Client/Acount created successfully').should('exist');
  cy.get('.toast-message').should('not.exist');
});

describe('Test bank transactions', () => {
  it('Stewie Griffin should end up with balance equal to $700 CAD', () => {
    cy.get('button').contains('Deposit').click();
    cy.get('.modals').should('exist');
    cy.get('.input [name="accountId"]').type('1234');
    cy.get('.input [name="amount"]').type('300');
    cy.get('i.dropdown').click();
    cy.get('[role="option"]').contains('USD').click();
    cy.get('button').contains('Submit').click();
    cy.get('.toast-message').contains('Deposit completed successfully').should('exist');
    cy.get('button').contains('Account Information').click();
    cy.get('.modals').should('exist');
    cy.get('.input [name="accountId"]').type('1234');
    cy.get('button').contains('Submit').click();
    cy.get('h3').should('contain', '$700');
  });
});
