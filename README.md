# README #

This README document provides the needed instructions to get the application up and running.

### System Requirements ###
* This application has been only tested against Windows machine, It's highly recommended to use Windows 10 operating system

### Installation ###

* Clone the repository [bank](https://bitbucket.org/khaledirk/bank/src/master/) into your local drive
* Open a terminal, change directory to the project folder, then type the following to install the app (This may take some time as Cypress is being installed)
> npm i
* Run the application using the following command (Browse to http://localhost:1337)
> npm start
* Run the ui test using command (This may some time to start Cypress run)
> npm test
* Run the api test using command
> npm run test:api

### Endpoints ###

Method | Request Headers | Endpoint | Request Body
------ | ------- | ---------| ------------
GET | N/A | /api/clients/{clientid} | N/A
GET | N/A | /api/accounts/{accountid} | N/A
PUT | N/A | /api/withdraw/{clientid}/{accountid}?amount={amount}&currency={CAD/USD/MXN}
PUT | N/A | /api/deposit/{accountid}?amount={amount}&currency={CAD/USD/MXN}
PUT | N/A | /api/transfer/{clientid}/{fromaccountid}/{toaccountid}?amount={amount}&currency={CAD/USD/MXN}
PUT | N/A | /api/associate/{clientid}/{accountid}
POST | content-type: application/json | /api/clients | { "clientId": "{clientid}", "name": "{name}", "email": "{email}", "phoneNumber": "{phoneNumber}" }
POST | content-type: application/json | /api/accounts | { "accountId": "{accountId}", "balance": {balance} }
