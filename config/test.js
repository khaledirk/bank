import bunyan from 'bunyan';

// Load package.json
const pjs = require('../package.json');

// Get some meta info from the package.json
const { name, version } = pjs;

// Set up a logger
const getLogger = (serviceName, serviceVersion, level) => bunyan.createLogger({ name: `${serviceName}:${serviceVersion}`, level });

// Configuration options for test
module.exports = {
  name,
  version,
  port: 1338,
  log: () => getLogger(name, version, 'fatal'),
  exchangeRate: { USD: 0.5, MXN: 10, CAD: 1 }
};
