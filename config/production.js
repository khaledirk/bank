import bunyan from 'bunyan';

// Load package.json
const pjs = require('../package.json');

// Get some meta info from the package.json
const { name, version } = pjs;

// Set up a logger
const getLogger = (serviceName, serviceVersion, level) => bunyan.createLogger({ name: `${serviceName}:${serviceVersion}`, level });

// Configuration options for production
module.exports = {
  name,
  version,
  port: 80,
  log: () => getLogger(name, version, 'info'),
  exchangeRate: { USD: 0.5, MXN: 10, CAD: 1 }
};
