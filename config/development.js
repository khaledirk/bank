import bunyan from 'bunyan';

// Load package.json
const pjs = require('../package.json');

// Get some meta info from the package.json
const { name, version } = pjs;

// Set up a logger
const getLogger = (serviceName, serviceVersion, level) => bunyan.createLogger({ name: `${serviceName}:${serviceVersion}`, level });

// Configuration options for development
module.exports = {
  name,
  version,
  port: 1337,
  log: () => getLogger(name, version, 'debug'),
  exchangeRate: { USD: 0.5, MXN: 10, CAD: 1 }
};
