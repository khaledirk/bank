const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { version } = require('../package.json');

const rootPath = path.resolve(__dirname + '/..');
const srcPath = path.join(rootPath, 'client');
const outputPath = path.join(rootPath, '/public');

const backEndPort = 1337;

module.exports = function (env) {
  return {
    mode: 'production',
    context: srcPath,
    optimization: {
      minimize: true,
    },
    devServer: {
      proxy: {
        '/favicon.ico': `http://localhost:${backEndPort}`,
        '/css': `http://localhost:${backEndPort}`,
        '/api': `http://localhost:${backEndPort}`,
        '/socketio': {
          target: `http://localhost:${backEndPort}/socket.io`,
          ws: true,
        },
      },
      contentBase: path.resolve(srcPath),
      host: '0.0.0.0',
      hot: true,
      historyApiFallback: true,
      disableHostCheck: true,
    },
    resolve: {
      extensions: ['.js', '.jsx'],
      modules: [
        srcPath,
        'node_modules',
      ],
    },
    entry: [
      'babel-polyfill',
      'main.jsx',
    ],

    output: {
      publicPath: '/',
      path: outputPath,
      filename: 'js/bundle.[hash].min.js',
    },

    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          use: ['babel-loader', 'eslint-loader'],
          exclude: /node_modules/,
        },
        {
          test: /\.css$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
              options: {
                hmr: false,
              },
            },
            'css-loader',
          ],
        },
        {
          test: /\.(ttf|eot|svg|woff|png|woff2)(\?[a-z0-9]+)?$/,
          loader: 'url-loader?name=[path][name].[ext]',
        },
        {
          test: /\.(png|jpg|gif)$/i,
          use: [
            {
              loader: 'url-loader',
              options: {
                limit: 8192,
              },
            },
          ],
        },
      ],
    },

    plugins: [
      new webpack.DefinePlugin({ 'pocess.env': { NODE_ENV: JSON.stringify(env) } }),
      new CopyWebpackPlugin({
        patterns: [{ from: 'assets/', to: 'assets' }]
      }),
      new CleanWebpackPlugin({
        cleanOnceBeforeBuildPatterns: ['public/js', 'public/css'],
        root: rootPath,
        verbose: true,
      }),
      new CopyWebpackPlugin({
        patterns: [{ from: '../node_modules/semantic-ui-css/semantic.min.css', to: 'css/semantic.min.css' }],
      }),
      new CopyWebpackPlugin({
        patterns: [{ from: '../node_modules/semantic-ui-css/themes', to: 'css/themes' }]
      }),
      new MiniCssExtractPlugin({
        // Options similar to the same options in webpackOptions.output
        // both options are optional
        filename: '[name].[hash].css',
        chunkFilename: '[id].[hash].css',
      }),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NamedModulesPlugin(),
      new webpack.NoEmitOnErrorsPlugin(),
      new HtmlWebpackPlugin({ template: 'index.ejs', title: `One Bank ${version}`, }),
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
      }),
    ],
  };
};
