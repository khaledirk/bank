function buildConfig(env) {
  let nodeEnv = process.env.NODE_ENV || env || 'development';
  if (!process.env.NODE_ENV) {
    process.env.NODE_ENV = nodeEnv;
  }

  if (env === 'test') {
    nodeEnv = 'development';
  }


  /* eslint-disable global-require */
  if (nodeEnv === 'production') {
    return require('./webpack_config/production')(env);
  }
  return require('./webpack_config/development')(env);
}

module.exports = buildConfig;
